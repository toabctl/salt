From 0fc45e0404f63be9afdcabf2434bde8ca12e68ea Mon Sep 17 00:00:00 2001
From: Benjamin Drung <benjamin.drung@profitbricks.com>
Date: Wed, 21 Feb 2018 18:56:48 +0100
Subject: [PATCH 5/5] Fix osfinger grain on Debian

Rework _run_os_grains_tests() to check all os grains including osfinger.
Fix osfinger on Debian (e.g. use "Debian-9" instead of
"Debian GNU/Linux-9").

Signed-off-by: Benjamin Drung <benjamin.drung@profitbricks.com>
Forwarded: https://github.com/saltstack/salt/pull/46139
---
 salt/grains/core.py            |   2 +-
 tests/unit/grains/test_core.py | 113 +++++++++++++++++++++++++----------------
 2 files changed, 70 insertions(+), 45 deletions(-)

diff --git a/salt/grains/core.py b/salt/grains/core.py
index cbd95dbd68..26a87d5d82 100644
--- a/salt/grains/core.py
+++ b/salt/grains/core.py
@@ -1691,7 +1691,7 @@ def os_data():
                 grains['osrelease_info']
             )
         os_name = grains['os' if grains.get('os') in (
-            'FreeBSD', 'OpenBSD', 'NetBSD', 'Mac', 'Raspbian') else 'osfullname']
+            'Debian', 'FreeBSD', 'OpenBSD', 'NetBSD', 'Mac', 'Raspbian') else 'osfullname']
         grains['osfinger'] = '{0}-{1}'.format(
             os_name, grains['osrelease'] if os_name in ('Ubuntu',) else grains['osrelease_info'][0])
 
diff --git a/tests/unit/grains/test_core.py b/tests/unit/grains/test_core.py
index 020a8a4373..c2f9070a4d 100644
--- a/tests/unit/grains/test_core.py
+++ b/tests/unit/grains/test_core.py
@@ -209,7 +209,7 @@ class CoreGrainsTestCase(TestCase, LoaderModuleMockMixin):
         self.assertEqual(os_grains.get('os_family'), 'Suse')
         self.assertEqual(os_grains.get('os'), 'SUSE')
 
-    def _run_os_grains_tests(self, os_release_map):
+    def _run_os_grains_tests(self, os_release_map, expectation):
         path_isfile_mock = MagicMock(side_effect=lambda x: x in os_release_map['files'])
         empty_mock = MagicMock(return_value={})
         osarch_mock = MagicMock(return_value="amd64")
@@ -256,19 +256,16 @@ class CoreGrainsTestCase(TestCase, LoaderModuleMockMixin):
                                                     with patch.dict(core.__salt__, {'cmd.run': osarch_mock}):
                                                         os_grains = core.os_data()
 
-        self.assertEqual(os_grains.get('os'), os_release_map['os'])
-        self.assertEqual(os_grains.get('os_family'), os_release_map['os_family'])
-        self.assertEqual(os_grains.get('osfullname'), os_release_map['osfullname'])
-        self.assertEqual(os_grains.get('oscodename'), os_release_map['oscodename'])
-        self.assertEqual(os_grains.get('osrelease'), os_release_map['osrelease'])
-        self.assertListEqual(list(os_grains.get('osrelease_info')), os_release_map['osrelease_info'])
-        self.assertEqual(os_grains.get('osmajorrelease'), os_release_map['osmajorrelease'])
+        grains = {k: v for k, v in os_grains.items()
+                  if k in set(["os", "os_family", "osfullname", "oscodename", "osfinger",
+                               "osrelease", "osrelease_info", "osmajorrelease"])}
+        self.assertEqual(grains, expectation)
 
-    def _run_suse_os_grains_tests(self, os_release_map):
+    def _run_suse_os_grains_tests(self, os_release_map, expectation):
         os_release_map['linux_distribution'] = ('SUSE test', 'version', 'arch')
-        os_release_map['os'] = 'SUSE'
-        os_release_map['os_family'] = 'Suse'
-        self._run_os_grains_tests(os_release_map)
+        expectation['os'] = 'SUSE'
+        expectation['os_family'] = 'Suse'
+        self._run_os_grains_tests(os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_suse_os_grains_sles11sp3(self):
@@ -280,14 +277,17 @@ class CoreGrainsTestCase(TestCase, LoaderModuleMockMixin):
 VERSION = 11
 PATCHLEVEL = 3
 ''',
+            'files': ["/etc/SuSE-release"],
+        }
+        expectation = {
             'oscodename': 'SUSE Linux Enterprise Server 11 SP3',
             'osfullname': "SLES",
             'osrelease': '11.3',
-            'osrelease_info': [11, 3],
+            'osrelease_info': (11, 3),
             'osmajorrelease': 11,
-            'files': ["/etc/SuSE-release"],
+            'osfinger': 'SLES-11',
         }
-        self._run_suse_os_grains_tests(_os_release_map)
+        self._run_suse_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_suse_os_grains_sles11sp4(self):
@@ -304,14 +304,17 @@ PATCHLEVEL = 3
                 'ANSI_COLOR': '0;32',
                 'CPE_NAME': 'cpe:/o:suse:sles:11:4'
             },
+            'files': ["/etc/os-release"],
+        }
+        expectation = {
             'oscodename': 'SUSE Linux Enterprise Server 11 SP4',
             'osfullname': "SLES",
             'osrelease': '11.4',
-            'osrelease_info': [11, 4],
+            'osrelease_info': (11, 4),
             'osmajorrelease': 11,
-            'files': ["/etc/os-release"],
+            'osfinger': 'SLES-11',
         }
-        self._run_suse_os_grains_tests(_os_release_map)
+        self._run_suse_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_suse_os_grains_sles12(self):
@@ -328,14 +331,17 @@ PATCHLEVEL = 3
                 'ANSI_COLOR': '0;32',
                 'CPE_NAME': 'cpe:/o:suse:sles:12'
             },
+            'files': ["/etc/os-release"],
+        }
+        expectation = {
             'oscodename': 'SUSE Linux Enterprise Server 12',
             'osfullname': "SLES",
             'osrelease': '12',
-            'osrelease_info': [12],
+            'osrelease_info': (12,),
             'osmajorrelease': 12,
-            'files': ["/etc/os-release"],
+            'osfinger': 'SLES-12',
         }
-        self._run_suse_os_grains_tests(_os_release_map)
+        self._run_suse_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_suse_os_grains_sles12sp1(self):
@@ -352,14 +358,17 @@ PATCHLEVEL = 3
                 'ANSI_COLOR': '0;32',
                 'CPE_NAME': 'cpe:/o:suse:sles:12:sp1'
             },
+            'files': ["/etc/os-release"],
+        }
+        expectation = {
             'oscodename': 'SUSE Linux Enterprise Server 12 SP1',
             'osfullname': "SLES",
             'osrelease': '12.1',
-            'osrelease_info': [12, 1],
+            'osrelease_info': (12, 1),
             'osmajorrelease': 12,
-            'files': ["/etc/os-release"],
+            'osfinger': 'SLES-12',
         }
-        self._run_suse_os_grains_tests(_os_release_map)
+        self._run_suse_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_suse_os_grains_opensuse_leap_42_1(self):
@@ -376,14 +385,17 @@ PATCHLEVEL = 3
                 'ANSI_COLOR': '0;32',
                 'CPE_NAME': 'cpe:/o:opensuse:opensuse:42.1'
             },
+            'files': ["/etc/os-release"],
+        }
+        expectation = {
             'oscodename': 'openSUSE Leap 42.1 (x86_64)',
             'osfullname': "Leap",
             'osrelease': '42.1',
-            'osrelease_info': [42, 1],
+            'osrelease_info': (42, 1),
             'osmajorrelease': 42,
-            'files': ["/etc/os-release"],
+            'osfinger': 'Leap-42',
         }
-        self._run_suse_os_grains_tests(_os_release_map)
+        self._run_suse_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_suse_os_grains_tumbleweed(self):
@@ -400,14 +412,17 @@ PATCHLEVEL = 3
                 'ANSI_COLOR': '0;32',
                 'CPE_NAME': 'cpe:/o:opensuse:opensuse:20160504'
             },
+            'files': ["/etc/os-release"],
+        }
+        expectation = {
             'oscodename': 'openSUSE Tumbleweed (20160504) (x86_64)',
             'osfullname': "Tumbleweed",
             'osrelease': '20160504',
-            'osrelease_info': [20160504],
+            'osrelease_info': (20160504,),
             'osmajorrelease': 20160504,
-            'files': ["/etc/os-release"],
+            'osfinger': 'Tumbleweed-20160504',
         }
-        self._run_suse_os_grains_tests(_os_release_map)
+        self._run_suse_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_ubuntu_xenial_os_grains(self):
@@ -430,14 +445,16 @@ PATCHLEVEL = 3
                 'VERSION_CODENAME': 'xenial',
                 'UBUNTU_CODENAME': 'xenial',
             },
+        }
+        expectation = {
             'oscodename': 'xenial',
             'osfullname': 'Ubuntu',
             'osrelease': '16.04',
-            'osrelease_info': [16, 4],
+            'osrelease_info': (16, 4),
             'osmajorrelease': 16,
             'osfinger': 'Ubuntu-16.04',
         }
-        self._run_ubuntu_os_grains_tests(_os_release_map)
+        self._run_ubuntu_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_ubuntu_artful_os_grains(self):
@@ -462,14 +479,16 @@ PATCHLEVEL = 3
                 'VERSION_CODENAME': 'artful',
                 'UBUNTU_CODENAME': 'artful',
             },
+        }
+        expectation = {
             'oscodename': 'artful',
             'osfullname': 'Ubuntu',
             'osrelease': '17.10',
-            'osrelease_info': [17, 10],
+            'osrelease_info': (17, 10),
             'osmajorrelease': 17,
             'osfinger': 'Ubuntu-17.10',
         }
-        self._run_ubuntu_os_grains_tests(_os_release_map)
+        self._run_ubuntu_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_debian_7_os_grains(self):
@@ -491,16 +510,18 @@ PATCHLEVEL = 3
                 'SUPPORT_URL': "http://www.debian.org/support/",
                 'BUG_REPORT_URL': "https://bugs.debian.org/",
             },
+        }
+        expectation = {
             'os': 'Debian',
             'os_family': 'Debian',
             'oscodename': 'wheezy',
             'osfullname': 'Debian GNU/Linux',
             'osrelease': '7',
-            'osrelease_info': [7],
+            'osrelease_info': (7,),
             'osmajorrelease': 7,
             'osfinger': 'Debian-7',
         }
-        self._run_os_grains_tests(_os_release_map)
+        self._run_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_debian_8_os_grains(self):
@@ -521,16 +542,18 @@ PATCHLEVEL = 3
                 'SUPPORT_URL': "http://www.debian.org/support",
                 'BUG_REPORT_URL': "https://bugs.debian.org/",
             },
+        }
+        expectation = {
             'os': 'Debian',
             'os_family': 'Debian',
             'oscodename': 'jessie',
             'osfullname': 'Debian GNU/Linux',
             'osrelease': '8',
-            'osrelease_info': [8],
+            'osrelease_info': (8,),
             'osmajorrelease': 8,
             'osfinger': 'Debian-8',
         }
-        self._run_os_grains_tests(_os_release_map)
+        self._run_os_grains_tests(_os_release_map, expectation)
 
     @skipIf(not salt.utils.is_linux(), 'System is not Linux')
     def test_debian_9_os_grains(self):
@@ -551,22 +574,24 @@ PATCHLEVEL = 3
                 'SUPPORT_URL': "https://www.debian.org/support",
                 'BUG_REPORT_URL': "https://bugs.debian.org/",
             },
+        }
+        expectation = {
             'os': 'Debian',
             'os_family': 'Debian',
             'oscodename': 'stretch',
             'osfullname': 'Debian GNU/Linux',
             'osrelease': '9',
-            'osrelease_info': [9],
+            'osrelease_info': (9,),
             'osmajorrelease': 9,
             'osfinger': 'Debian-9',
         }
-        self._run_os_grains_tests(_os_release_map)
+        self._run_os_grains_tests(_os_release_map, expectation)
 
-    def _run_ubuntu_os_grains_tests(self, os_release_map):
-        os_release_map['os'] = 'Ubuntu'
-        os_release_map['os_family'] = 'Debian'
+    def _run_ubuntu_os_grains_tests(self, os_release_map, expectation):
+        expectation['os'] = 'Ubuntu'
+        expectation['os_family'] = 'Debian'
         os_release_map['files'] = '/etc/os-release'
-        self._run_os_grains_tests(os_release_map)
+        self._run_os_grains_tests(os_release_map, expectation)
 
     def test_docker_virtual(self):
         '''
-- 
2.14.1

